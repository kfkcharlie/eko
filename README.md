### Przykładowe zadanie z wykorzystaniem framework'a Symfony

Aplikacja wykonana za pomocą framework'a Symfony 4 oraz bazy danych MariaDB 10.


## Założenia aplikacji - skłdanie propozycji nowego przystanku.

1. Formularz zgłoszenia propozycji nowego przystanku. W formularzu zawarte są następujące pola:
    - tytuł „Zaproponuj przystanek” bez możliwości edycji
    - adres proponowanego przystanku
    - opis 
    - możliwość dołączenia kilku (max 3) załączników z rozszerzeniami popularnych plików obrazkowych. Załączniki przechowywane są w katalogu: app/public/uploads

Aplikacja składa się z trzech głównych części:
- formularza zgłoszeniowego
- formularza logowanie
- panelu administratora

Założenia:
- panel administratora - możliwość podglądu zgłoszonych propozycji z
możliwością rozwinięcia szczegółów. Ponadto przeczytanie danej propozycji jest zapisywane w bazie za pomocą flagi, udostępnionej w postaci informacji wizualnej.
- formularz informuje użytkownika o dozwolonych rozszerzeniach załącznika oraz o tym, że proponowany przystanek jest polem obowiązkowym.

## Zrzuty ekranu przedstawiające działanie aplikacji

# Formularz zgłoszeniowy
![zgloszenie](http://gitlab.com/kfkcharlie/eko/-/raw/master/ekosymfonia/public/imgReadme/zg%C5%82oszenie.PNG)

# Formularz logowania
![logowanie](http://gitlab.com/kfkcharlie/eko/-/raw/master/ekosymfonia/public/imgReadme/logowanie.PNG) 

# Panel administratora - lista propozycji
![lista propozycji](http://gitlab.com/kfkcharlie/eko/-/raw/master/ekosymfonia/public/imgReadme/lista.PNG)

# Panel administratora - szczegółowe informacje
![pojedynczy wpis](http://gitlab.com/kfkcharlie/eko/-/raw/master/ekosymfonia/public/imgReadme/wpis.PNG)


## Podsumowanie

Aplikacja miała za zadanie możliwie najlepiej spełnić wymagania zapisane w mailu.
Kierunki poprawy funkcjonalności działania aplikacji, w celu poprawy jej jakości:

- Połączenie wypełnionego formularza z użytkownikiem zapisanym w bazie
- Rozbudowa aplikacji o warstwę zarządzającą przesyłanymi plikami jako użytkowanikami o większych uprawnieniach z różnymi poziomami dostępu
