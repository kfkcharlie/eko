<?php

namespace App\Entity;

use App\Repository\StationsRepository;
use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=StationsRepository::class)
 */
class Stations
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="integer")
     */
    private $idUser;

    /**
     * @Assert\Regex(
     *     pattern="/^([0-9]{1,5}[ ]{0,1}[-]{0,1}[a-zA-Z]{1,200})|([a-zA-Z. -]{1,200}[0-9]{0,5})$/",
     *     message="Nazwa ulicy może składać się tylko z liter i cyfr"
     * )
     * @ORM\Column(type="string", length=255)
     */
    private $street;
    
    /**
     * @Assert\Regex(
     *     pattern="/^(\d{1,4}[a-z]{0,1})$/",
     *     message="Numer budynku może składać się z maksymalnie 3 liczb i tylko jednej litery na końcu, np. 1, 4, 6a, 7B"
     * )
     * @ORM\Column(type="string", length=10)
     */
    private $nrBuilding;

    /**
     * @Assert\Regex(
     *     pattern="/^([0-9]{2}-[0-9]{3})$/",
     *     message="Wzór prawidłowego kodu pocztowego: 00-000"
     * )
     * @ORM\Column(type="string", length=10)
     */
    private $zipCode;

    /**
     * @Assert\Regex(
     *     pattern="/^([A-Za-z .-]{3,100})$/u",
     *     message="Nazwa miasta może składać się tylko z liter"
     * )
     * @ORM\Column(type="string", length=255)
     */
    private $city;

    /**
     * @ORM\Column(type="integer")
     */
    private $readed;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $img1;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $img2;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $img3;

    /**
     * @Assert\Regex(
     *     pattern="/^[A-Z0-9 .,ŚśŁłśÓóĆćŃńĘęŻżŹźĄą]{0,200}$/im",
     *     message="Opis może składać się maksymalnie z 200 znaków, liter lub cyfr"
     * )
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $description;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $title;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getIdUser(): ?int
    {
        return $this->idUser;
    }

    public function setIdUser(int $idUser): self
    {
        $this->idUser = $idUser;

        return $this;
    }

    public function getStreet(): ?string
    {
        return $this->street;
    }

    public function setStreet(string $street): self
    {
        $this->street = $street;

        return $this;
    }

    public function getZipCode(): ?string
    {
        return $this->zipCode;
    }

    public function setZipCode(string $zipCode): self
    {
        $this->zipCode = $zipCode;

        return $this;
    }

    public function getCity(): ?string
    {
        return $this->city;
    }

    public function setCity(string $city): self
    {
        $this->city = $city;

        return $this;
    }

    public function getReaded(): ?int
    {
        return $this->readed;
    }

    public function setReaded(int $readed): self
    {
        $this->readed = $readed;

        return $this;
    }

    public function getImg1(): ?string
    {
        return $this->img1;
    }

    public function setImg1(?string $img1): self
    {
        $this->img1 = $img1;

        return $this;
    }

    public function getImg2(): ?string
    {
        return $this->img2;
    }

    public function setImg2(?string $img2): self
    {
        $this->img2 = $img2;

        return $this;
    }

    public function getImg3(): ?string
    {
        return $this->img3;
    }

    public function setImg3(?string $img3): self
    {
        $this->img3 = $img3;

        return $this;
    }

    public function getNrBuilding(): ?string
    {
        return $this->nrBuilding;
    }

    public function setNrBuilding(string $nrBuilding): self
    {
        $this->nrBuilding = $nrBuilding;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(?string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function getTitle(): ?string
    {
        return $this->title;
    }

    public function setTitle(?string $title): self
    {
        $this->title = $title;

        return $this;
    }
}
