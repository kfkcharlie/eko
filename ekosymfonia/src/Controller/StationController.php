<?php

namespace App\Controller;

use App\Entity\Stations;
use App\Form\AddStationType;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class StationController extends AbstractController
{
    /**
     * @Route("/", name="addstation")
     */
    public function add(EntityManagerInterface $em, Request $request)
    {
        
        $form = $this->createForm(AddStationType::class);
        
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $data = $form->getData();
            // Zapisywanie zdjęć
            $img = $this->imgValidations($request);
            if ($img['error']=='')
            {
                $station = new Stations();
                $station->setIdUser(1);
                $station->setReaded(0);
                $station->setImg1($img['img1']);
                $station->setImg2($img['img2']);
                $station->setImg3($img['img3']);
                $station->setStreet($data->getStreet());
                $station->setNrBuilding($data->getNrBuilding());
                $station->setZipCode($data->getZipCode());
                $station->setCity($data->getCity());
                $station->setTitle($data->getTitle());
                $station->setDescription($data->getDescription());

                $em->persist($station);
                $em->flush();
                return $this->redirectToRoute('info');
            }
            else {
                return $this->render('station/index.html.twig', [
                    'articleForm' => $form->createView(),
                    'message'=> 'Dozwolone formaty plików: jpg, jpeg, png!',
                ]);
            }
        }
        return $this->render('station/index.html.twig', [
            'articleForm' => $form->createView(),
            'message'=> 'tom',
        ]);
    

    }

    /**
     * @Route("/info", name="info")
     */
    public function info()
    {        
        return $this->render('station/info.html.twig');
    }

    /**
     * @Route("/station", name="admin")
     */
    public function station()
    {    $station = $this->getDoctrine()
        ->getRepository(Stations::class)
        ->findAll(); 
        return $this->render('station/list.html.twig', [
            'stations'=>$station
        ]);
    }

    /**
     * @Route("/station/{id}", name="show")
     */
    public function stationId(EntityManagerInterface $em, $id)
    {    $station = $this->getDoctrine()
        ->getRepository(Stations::class)
        ->findBy(["id"=>$id]); 
        $station[0]->setReaded(1);

        $em->persist($station[0]);
        $em->flush();

        return $this->render('station/station.html.twig', [
            'station'=>$station[0]
        ]);
    }

    protected function imgValidations($request)
    {
        // dd( $request->files->get('add_station') );
        $return = [
            'error' => '',
            'img1'=>'',
            'img2'=>'',
            'img3'=>''
        ];
        $file1 = '';
        $file2 = '';
        $file3 = '';
        
        if( $request->files->get('add_station')['img1'] != null)
        {
            if( !$this->checkImg($request->files->get('add_station')['img1']) )
                $file1 = $request->files->get('add_station')['img1'];
            else $return['error'] = $return['error'] . '1'; 
        }
        if( $request->files->get('add_station')['img2'] != null)
        {
            if( !$this->checkImg($request->files->get('add_station')['img2']) )
                $file2 = $request->files->get('add_station')['img2'];
            else $return['error'] = $return['error'] . '2'; 
        }

        if( $request->files->get('add_station')['img3'] != null)
        {
            if( !$this->checkImg($request->files->get('add_station')['img3']) )
                $file3 = $request->files->get('add_station')['img3'];
            else $return['error'] = $return['error'] . '3'; 
        }

        if($return['error'] == '')
        {
            if($file1 != '')
                $return['img1'] = $this->saveImg($file1);
            if($file2 != '')
                $return['img2'] = $this->saveImg($file2);
            if($file3 != '')
                $return['img3'] = $this->saveImg($file3);
        }

        return $return;
    }

    protected function checkImg($file)
    {
        if($file->guessExtension() == 'jpg' || $file->guessExtension() == 'jpeg' || $file->guessExtension() == 'png' || $file->guessExtension() == 'JPG' || $file->guessExtension() == 'JPEG' || $file->guessExtension() == 'PNG')
            return 0;
        else return 1;
    }

    protected function saveImg($file)
    {
        $fileName = md5(uniqid()) . '.' . $file->guessExtension();
        $file->move(
            $this->getParameter('uploads_directory'),
            $fileName
        );
        return $fileName;
    }


}
