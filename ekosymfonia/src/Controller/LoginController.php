<?php

namespace App\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;
/**
 * Class LoginController
 * @package App\Controller
 */
class LoginController extends AbstractController {
  /**
   * @Route("/login", name="login")
   */
  public function login(Request $request, AuthenticationUtils $authenticationUtils) : Response {
    $errors = $authenticationUtils->getLastAuthenticationError();
    $lastUsername = $authenticationUtils->getLastUsername();
    return $this->render('User/login.html.twig', [
      'errors' => $errors,
      'username' => $lastUsername
    ]);
  }
  /**
   * @Route("/logout", name="logout")
   */
  public function logout() {}
}
