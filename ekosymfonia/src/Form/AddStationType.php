<?php

namespace App\Form;

use App\Entity\Stations;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class AddStationType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('title', TextType::class,
            [ "label"=>"Zaproponuj przystanek"
            , "disabled" => true ])
            ->add('street', TextType::class,
            [ "label"=>"* Ulica"])            
            ->add('nrBuilding', TextType::class,
            [ "label"=>"* Numer budynku"])
            ->add('zipCode', TextType::class,
            [ "label"=>"* Kod pocztowy"]
            )
            ->add('city', TextType::class,
            [ "label"=>"* Miasto"])
            ->add('description', TextareaType::class,
            [ "label"=>"Opis"
            , "required" => false])
            ->add('img1', FileType::class,
            [ "label"=>"Zdjęcie 1"
            , 'mapped'=>false
            , 'required'=>false])
            ->add('img2', FileType::class,
            [ "label"=>"Zdjęcie 2"
            , 'mapped'=>false
            , 'required'=>false])
            ->add('img3', FileType::class,
            [ "label"=>"Zdjęcie 3"
            , 'mapped'=>false
            , 'required'=>false])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Stations::class,
        ]);
    }
}
